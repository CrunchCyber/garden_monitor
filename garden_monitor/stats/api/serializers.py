from rest_framework import serializers
from stats.models import MonthlyWeatherStats, WaterUseStatistic


class MonthlyWeatherStatsSerializer(serializers.ModelSerializer):
    """Serializer for monthly weather stats"""
    class Meta:
        model = MonthlyWeatherStats
        fields = "__all__"


class WaterUseStatisticSerializer(serializers.ModelSerializer):
    """Serializer for monthly water usage"""
    class Meta:
        model = WaterUseStatistic
        fields = "__all__"
