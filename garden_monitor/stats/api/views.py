from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from stats.models import MonthlyWeatherStats, WaterUseStatistic
from stats.api.serializers import MonthlyWeatherStatsSerializer, WaterUseStatisticSerializer


class MonthlyWeatherStatsAPIView(generics.ListCreateAPIView):
    queryset = MonthlyWeatherStats.objects.all()
    serializer_class = MonthlyWeatherStatsSerializer


class MonthlyWeatherStatsByYearAPIView(APIView):
    serializer_class = MonthlyWeatherStatsSerializer

    def get(self, request, year):
        data = MonthlyWeatherStats.objects.filter(date__year=year)
        serializer = MonthlyWeatherStatsSerializer(data, many=True)
        return Response(serializer.data)


class WaterUseStatisticAPIView(generics.ListCreateAPIView):
    queryset = WaterUseStatistic.objects.all()
    serializer_class = WaterUseStatisticSerializer


class WaterUseStatisticByYearAPIView(APIView):
    serializer_class = WaterUseStatistic

    def get(self, request, year):
        data = WaterUseStatistic.objects.filter(date__year=year)
        serializer = WaterUseStatisticSerializer(data, many=True)
        return Response(serializer.data)

class WaterUseStatisticByYearMonthAPIView(APIView):
    serializer_class = WaterUseStatistic

    def get(self, request, year, month):
        data = WaterUseStatistic.objects.filter(date__year=year, date__month=month)
        serializer = WaterUseStatisticSerializer(data, many=True)
        return Response(serializer.data)
