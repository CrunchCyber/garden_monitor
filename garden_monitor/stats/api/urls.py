from django.urls import path
from stats.api.views import MonthlyWeatherStatsAPIView, MonthlyWeatherStatsByYearAPIView

urlpatterns = [
    path('statistics/weather/', MonthlyWeatherStatsAPIView.as_view(), name='weather-stats'),
    path('statistics/weather/<int:year>/', MonthlyWeatherStatsByYearAPIView.as_view(), name='year-weather-stats')
]
