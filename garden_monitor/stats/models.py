from django.db import models

# Create your models here.
class MonthlyWeatherStats(models.Model):
    date = models.DateTimeField()
    temperature_max = models.FloatField()
    temperature_median = models.FloatField()
    temperature_min = models.FloatField()
    rain_amount = models.FloatField()
    rain_days = models.IntegerField()
    lightning_strikes = models.IntegerField()

    def __str__(self):
        return f'Report for {self.date}'


class WaterUseStatistic(models.Model):
    bed = models.ForeignKey('beds.Bed', on_delete=models.CASCADE)
    date = models.DateTimeField()
    water_amount = models.FloatField()

    def __str__(self):
        return f'{self.bed.bed_name} water use'
