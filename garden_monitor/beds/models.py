from django.db import models


class BedType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Bed(models.Model):
    bed_name = models.CharField(max_length=50)
    type = models.ForeignKey(BedType, on_delete=models.CASCADE)
    notes = models.TextField(blank=True)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.bed_name


class BedMoisture(models.Model):
    moisture = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    sensor = models.ForeignKey('sensors.Sensor', on_delete=models.CASCADE)
    bed = models.ForeignKey('Bed', on_delete=models.CASCADE)

    def __str__(self):
        return f'Sensor for {self.bed.bed_name}'


class Vegetable(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    growPeriod = models.CharField(max_length=23)

    def __str__(self):
        return self.name


class PlantedVegetables(models.Model):
    planted_at = models.DateTimeField(auto_now_add=True)
    bed = models.ForeignKey(Bed, on_delete=models.CASCADE)
    vegetable = models.ForeignKey(Vegetable, on_delete=models.CASCADE)
    growing = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.vegetable.name} in bed {self.bed.bed_name}'


class HarvestedVegetable(models.Model):
    harvested_at = models.DateTimeField(auto_now_add=True)
    harvested_ammount = models.FloatField()
    planted_vegetable = models.ForeignKey(PlantedVegetables, on_delete=models.CASCADE)

    def __str__(self):
        return f'Harvested {self.planted_vegetable.vegetable.name} from bed {self.planted_vegetable.bed.bed_name}'
