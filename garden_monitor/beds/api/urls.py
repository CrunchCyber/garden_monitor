from django.urls import path
from beds.api.views import (VegetableAPIView, VegetableRUDAPIView,
                            BedAPIView, BedRUDAPIView,
                            BedTypeAPIView, BedTypeRUDAPIView,
                            MoistureByBedAPIView, MoistureAPIView,
                            MoistureByBedYearAPIView, MoistureByBedYearMonthAPIView,
                            MoistureByBedYearMonthDayAPIView, MoistureByBedLatestAPIView,
                            VegetablePlantedAPIView, VegetablePlantedRUDAPIView,
                            VegetablePlantedGrowingAPIView, VegetablePlantedByBedAPIView,
                            HarvestedVegetablesAPIView,)

urlpatterns = [
    # vegetables
    path('vegetables/', VegetableAPIView.as_view(), name='vegetable-list'),
    path('vegetables/<int:pk>/', VegetableRUDAPIView.as_view(), name='vegetable-detail'),
    path('vegetables/planted/', VegetablePlantedAPIView.as_view(), name='planted-vegetables'),
    path('vegetables/planted/growing/', VegetablePlantedGrowingAPIView.as_view(), name='planted-vegetables'),
    path('vegetables/planted/<int:pk>/', VegetablePlantedRUDAPIView.as_view(), name='planted-vegetables-detail'),
    path('vegetables/harvested/', HarvestedVegetablesAPIView.as_view(), name='harvested-vegetables-detail'),

    # paths for the bed endpoints, moisture POST has it's own endpoint outside of beds/
    path('beds/', BedAPIView.as_view(), name='bed-list'),
    path('beds/<int:pk>/', BedRUDAPIView.as_view(), name='bed-detail'),
    path('beds/<int:pk>/moisture/', MoistureByBedAPIView.as_view(), name='moisture-by-bed'),
    path('beds/<int:pk>/moisture/latest/', MoistureByBedLatestAPIView.as_view(), name='moisutre-by-bed-latest'),
    path('beds/<int:pk>/moisture/<int:year>/', MoistureByBedYearAPIView.as_view(), name='moisture-by-bed-year'),
    path('beds/<int:pk>/moisture/<int:year>/<int:month>/', MoistureByBedYearMonthAPIView.as_view(), name='moisture-by-bed-year-month'),
    path('beds/<int:pk>/moisture/<int:year>/<int:month>/<int:day>/', MoistureByBedYearMonthDayAPIView.as_view(), name='moisture-by-bed-year-month-day'),
    path('beds/<int:pk>/planted/', VegetablePlantedByBedAPIView.as_view(), name='planted-by-bed'),
    path('beds/types/', BedTypeAPIView.as_view(), name='bed-type-list'),
    path('beds/types/<int:pk>/', BedTypeRUDAPIView.as_view(), name='bed-type-detail'),
    # moisture
    path('moisture/', MoistureAPIView.as_view(), name='moisture-list')
]
