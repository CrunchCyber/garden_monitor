"""All views for the garden beds and vegetables"""
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from beds.models import (Bed, BedType, Vegetable,
                         PlantedVegetables, HarvestedVegetable,
                         BedMoisture)
from beds.api.serializers import (BedSerializer, BedTypeSerializer,
                         VegetableSerializer,
                         PlantedVegetablesSerializer, HarvestedVegetableSerializer,
                         BedMoistureSerializer)


#################
#
# Vegetables API Views
#
#################


class VegetableAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = Vegetable.objects.all().order_by('name')
    serializer_class = VegetableSerializer


class VegetableRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    queryset = Vegetable.objects.all()
    serializer_class = VegetableSerializer


#################
#
# Bed API Views
#
#################


class BedAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = Bed.objects.all()
    serializer_class = BedSerializer


class BedRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    queryset = Bed.objects.all()
    serializer_class = BedSerializer


class BedTypeAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = BedType.objects.all()
    serializer_class = BedTypeSerializer


class BedTypeRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    queryset = BedType.objects.all()
    serializer_class = BedTypeSerializer

#################
#
# Moisture API Views
#
#################


class MoistureAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint, includes all Moisture measurements taken"""
    queryset = BedMoisture.objects.all()
    serializer_class = BedMoistureSerializer


class MoistureByBedAPIView(APIView):
    """Returns Moisture measurements for one bed"""
    serializer_class = BedMoistureSerializer

    def get(self, request, pk):
        reports = BedMoisture.objects.filter(bed=pk).order_by('-created_at')
        serializer = BedMoistureSerializer(reports, many=True)
        return Response(serializer.data)


class MoistureByBedLatestAPIView(APIView):
    """Returns lates moisture measurment for one bed"""
    serializer_class = BedMoistureSerializer

    def get(self, request, pk):
        try:
            report = BedMoisture.objects.filter(bed=pk).order_by('-created_at')[0]
            serializer = BedMoistureSerializer(report)
            return Response(serializer.data)
        except IndexError:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class MoistureByBedYearAPIView(APIView):
    """Returns Moisture measurements for one bed and Year"""
    serializer_class = BedMoistureSerializer

    def get(self, request, pk, year):
        reports = BedMoisture.objects.filter(bed=pk, created_at__year=year)
        serializer = BedMoistureSerializer(reports, many=True)
        return Response(serializer.data)


class MoistureByBedYearMonthAPIView(APIView):
    """Returns Moisture measurements for one bed and Year/Month"""
    serializer_class = BedMoistureSerializer

    def get(self, request, pk, year, month):
        reports = BedMoisture.objects.filter(bed=pk, created_at__year=year, created_at__month=month)
        serializer = BedMoistureSerializer(reports, many=True)
        return Response(serializer.data)


class MoistureByBedYearMonthDayAPIView(APIView):
    """Returns Moisture measurements for one bed and Year/Month/Day"""
    serializer_class = BedMoistureSerializer

    def get(self, request, pk, year, month, day):
        reports = BedMoisture.objects.filter(bed=pk, created_at__year=year, created_at__month=month, created_at__day=day)
        serializer = BedMoistureSerializer(reports, many=True)
        return Response(serializer.data)


#################
#
# Moisture API Views
#
#################


class VegetablePlantedAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = PlantedVegetables.objects.all()
    serializer_class = PlantedVegetablesSerializer


class VegetablePlantedRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    queryset = PlantedVegetables.objects.all()
    serializer_class = PlantedVegetablesSerializer


class VegetablePlantedByBedAPIView(APIView):
    """ Returns List of planted vegetables per bed"""
    serializer_class = PlantedVegetablesSerializer

    def get(self, request, pk):
        planted = PlantedVegetables.objects.filter(bed=pk).order_by('-planted_at')
        serializer = PlantedVegetablesSerializer(planted, many=True)
        return Response(serializer.data)


class VegetablePlantedGrowingAPIView(APIView):
    """Returns list of all growing vegetables"""
    serializer_class = PlantedVegetablesSerializer

    def get(self, request):
        growing = PlantedVegetables.objects.filter(growing=True).order_by('-planted_at')
        serializer = PlantedVegetablesSerializer(growing, many=True)
        return Response(serializer.data)


class HarvestedVegetablesAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = HarvestedVegetable.objects.all()
    serializer_class = HarvestedVegetableSerializer
