from rest_framework import serializers
from beds.models import (Bed, BedType, Vegetable,
                         PlantedVegetables, HarvestedVegetable,
                         BedMoisture)


class BedSerializer(serializers.ModelSerializer):
    """Serializer for Beds"""
    class Meta:
        model = Bed
        fields = "__all__"


class BedTypeSerializer(serializers.ModelSerializer):
    """Serializer for Bed Types"""
    class Meta:
        model = BedType
        fields = "__all__"


class VegetableSerializer(serializers.ModelSerializer):
    """Serializer for Vegetables"""
    class Meta:
        model = Vegetable
        fields = "__all__"


class PlantedVegetablesSerializer(serializers.ModelSerializer):
    """Serializer for  planted Vegetables"""
    vegetable_name = serializers.SerializerMethodField('get_vegetable_name')
    growing_period = serializers.SerializerMethodField('get_growing_period')

    class Meta:
        model = PlantedVegetables
        fields = '__all__'

    def get_vegetable_name(self, obj):
        return obj.vegetable.name

    def get_growing_period(self, obj):
        return obj.vegetable.growPeriod

class HarvestedVegetableSerializer(serializers.ModelSerializer):
    """Serializer for  harvested Vegetables"""
    bed = serializers.SerializerMethodField('get_bed')

    class Meta:
        model = HarvestedVegetable
        fields = '__all__'

    def get_bed(self, obj):
        return obj.planted_vegetable.bed.id


class BedMoistureSerializer(serializers.ModelSerializer):
    """Serializer for Moisture"""
    class Meta:
        model = BedMoisture
        fields = "__all__"
