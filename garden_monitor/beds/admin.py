from django.contrib import admin
from beds.models import (Bed, BedType, Vegetable,
                         PlantedVegetables, HarvestedVegetable,
                         BedMoisture)

admin.site.register(Bed)
admin.site.register(BedType)
admin.site.register(Vegetable)
admin.site.register(PlantedVegetables)
admin.site.register(HarvestedVegetable)
admin.site.register(BedMoisture)
