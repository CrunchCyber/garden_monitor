from django.db import models


class Tank(models.Model):
    name = models.CharField(max_length=50)
    max = models.FloatField(default=0)
    min = models.FloatField(default=0)


class TankLevel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    level = models.FloatField()
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE)
    sensor = models.ForeignKey('sensors.Sensor', on_delete=models.CASCADE)


class Valve(models.Model):
    """Valves need more thinking"""
    name = models.CharField(max_length=50)
    active = models.BooleanField(default=False)
    sensor = models.ForeignKey('sensors.Sensor', on_delete=models.CASCADE)


class Flow(models.Model):
    """Flow need more thinking"""
    created_at = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField()
    valve = models.ForeignKey(Valve, on_delete=models.CASCADE)
