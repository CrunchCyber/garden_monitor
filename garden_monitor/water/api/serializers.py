from rest_framework import serializers
from water.models import Tank, TankLevel, Valve, Flow

class TankSerializer(serializers.ModelSerializer):
    """Serializer for rain water tanks"""
    class Meta:
        model = Tank
        fields = "__all__"


class TankLevelSerializer(serializers.ModelSerializer):
    """Serializer for rain water tank water level"""
    class Meta:
        model = TankLevel
        fields = "__all__"


class ValveSerializer(serializers.ModelSerializer):
    """Serializer for valves"""
    class Meta:
        model = Valve
        fields = "__all__"


class FlowSerializer(serializers.ModelSerializer):
    """Serializer for water flow"""
    class Meta:
        model = Flow
        fields = "__all__"
