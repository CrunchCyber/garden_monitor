from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from water.models import Tank, TankLevel, Valve, Flow
from water.api.serializers import (TankSerializer, TankLevelSerializer,
                                    ValveSerializer, FlowSerializer)


class TankAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = Tank.objects.all()
    serializer_class = TankSerializer


class TankRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    queryset = Tank.objects.all()
    serializer_class = TankSerializer


class TankLevelAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = TankLevel.objects.all()
    serializer_class = TankLevelSerializer


class TankLevelByTankAPIView(APIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    serializer_class = TankLevelSerializer

    def get(self, request, pk):
        reports = TankLevel.objects.filter(tank=pk).order_by('-created_at')
        serializer = TankLevelSerializer(reports, many=True)
        return Response(serializer.data)


class TankLevelByTankLatestAPIView(APIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    serializer_class = TankLevelSerializer

    def get(self, request, pk):
        report = TankLevel.objects.filter(tank=pk).order_by('-created_at')[0]
        serializer = TankLevelSerializer(report)
        return Response(serializer.data)


class TankLevelByTankYearAPIView(APIView):
    """Returns TankLevel measurements for one tank and Year"""
    serializer_class = TankLevelSerializer

    def get(self, request, pk, year):
        reports = TankLevel.objects.filter(tank=pk, created_at__year=year)
        serializer = TankLevelSerializer(reports, many=True)
        return Response(serializer.data)


class TankLevelByTankYearMonthAPIView(APIView):
    """Returns TankLevel measurements for one tank and Year/Month"""
    serializer_class = TankLevelSerializer

    def get(self, request, pk, year, month):
        reports = TankLevel.objects.filter(tank=pk, created_at__year=year, created_at__month=month)
        serializer = TankLevelSerializer(reports, many=True)
        return Response(serializer.data)


class TankLevelByTankYearMonthDayAPIView(APIView):
    """Returns TankLevel measurements for one tank and Year/Month/Day"""
    serializer_class = TankLevelSerializer

    def get(self, request, pk, year, month, day):
        reports = TankLevel.objects.filter(tank=pk, created_at__year=year, created_at__month=month, created_at__day=day)
        serializer = TankLevelSerializer(reports, many=True)
        return Response(serializer.data)


class ValveAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = Valve.objects.all()
    serializer_class = ValveSerializer


class ValveRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    queryset = Valve.objects.all()
    serializer_class = ValveSerializer


class FlowAPIView(generics.ListCreateAPIView):
    """ Returns List and Create API Endpoint"""
    queryset = Flow.objects.all()
    serializer_class = FlowSerializer


class FlowRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    """ Returns Retrieve Update Destroy for API Endpoint"""
    queryset = Flow.objects.all()
    serializer_class = FlowSerializer
