from django.urls import path
from water.api.views import (TankAPIView, TankRUDAPIView,
                             TankLevelAPIView, TankLevelByTankAPIView,
                             TankLevelByTankYearAPIView, TankLevelByTankYearMonthAPIView,
                             TankLevelByTankYearMonthDayAPIView, TankLevelByTankLatestAPIView,
                             ValveAPIView, ValveRUDAPIView,
                             FlowAPIView, FlowRUDAPIView)

urlpatterns = [
    path('tanks/', TankAPIView.as_view(), name='tank-list'),
    path('tanks/<int:pk>', TankRUDAPIView.as_view(), name='tank-details'),
    path('tanks/<int:pk>/level/', TankLevelByTankAPIView.as_view(), name='tank-level-list'),
    path('tanks/<int:pk>/level/latest/', TankLevelByTankLatestAPIView.as_view(), name='tank-level-latest'),
    path('tanks/<int:pk>/level/<int:year>/', TankLevelByTankYearAPIView.as_view(), name='tank-level-year'),
    path('tanks/<int:pk>/level/<int:year>/<int:month>/', TankLevelByTankYearMonthAPIView.as_view(), name='tank-level-year-month'),
    path('tanks/<int:pk>/level/<int:year>/<int:month>/<int:day>/', TankLevelByTankYearMonthDayAPIView.as_view(), name='tank-level-year-month-day'),
    path('tanks/level/', TankLevelAPIView.as_view(), name='tank-level-report'),
    path('valves/', ValveAPIView.as_view(), name='valve-list'),
    path('valves/<int:pk>', ValveRUDAPIView.as_view(), name='valve-detail'),
    path('flow/', FlowAPIView.as_view(), name='flow-list'),
    path('flow/<int:pk>', FlowRUDAPIView.as_view(), name='flow-detail'),
]