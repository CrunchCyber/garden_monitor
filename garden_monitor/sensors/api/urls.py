from django.urls import path
from sensors.api.views import (SensorAPIView, SensorRUDAPIView,
                               SensorTypeAPIView, SensorTypeRUDAPIView,
                               BatteryLevelAPIView, BatteryDetailView)

urlpatterns = [
    path('sensors/', SensorAPIView.as_view(), name='sensor-list'),
    path('sensors/<int:pk>/', SensorRUDAPIView.as_view(), name='sensor-detail'),
    path('sensors/types/', SensorTypeAPIView.as_view(), name='sensor-type-list'),
    path('sensors/types/<int:pk>/', SensorTypeRUDAPIView.as_view(), name='sensor-type-detail'),
    path('sensors/battery-level/', BatteryLevelAPIView.as_view(), name='sensor-list'),
    path('sensors/battery-level/<int:sensor>/', BatteryDetailView.as_view(), name='sensor-detail'),
]
