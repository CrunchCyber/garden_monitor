from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from sensors.models import Sensor, SensorType, BatteryLevel
from sensors.api.serializers import SensorSerializer, SensorTypeSerializer, BatteryLevelSerializer


class SensorAPIView(generics.ListCreateAPIView):
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer


class SensorRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer


class SensorTypeAPIView(generics.ListCreateAPIView):
    queryset = SensorType.objects.all()
    serializer_class = SensorTypeSerializer


class SensorTypeRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = SensorType.objects.all()
    serializer_class = SensorTypeSerializer


class BatteryLevelAPIView(generics.ListCreateAPIView):
    queryset = BatteryLevel.objects.all().order_by("-created_at")
    serializer_class = BatteryLevelSerializer


class BatteryDetailView(APIView):
    serializer_classes = BatteryLevelSerializer

    def get(self, request, sensor):
        voltage_levels = BatteryLevel.objects.filter(sensor=sensor).order_by("-created_at")
        serializer = BatteryLevelSerializer(voltage_levels, many=True)
        return Response(serializer.data)
