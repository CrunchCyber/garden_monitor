from rest_framework import serializers
from sensors.models import Sensor, SensorType, BatteryLevel


class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensor
        fields = "__all__"
        sensor_type = serializers.SerializerMethodField()

    def get_sensor_type(self, instance):
        return instance.type.filter(pk=instance.pk).exists()


class SensorTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorType
        fields = "__all__"


class BatteryLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = BatteryLevel
        fields = "__all__"
