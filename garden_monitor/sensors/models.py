from django.db import models


class SensorType(models.Model):
    type = models.CharField(max_length=50)

    def __str__(self):
        return self.type


class Sensor(models.Model):
    name = models.CharField(max_length=50)
    status = models.BooleanField(default=False)
    description = models.TextField()
    ip = models.CharField(max_length=15)
    type = models.ForeignKey(SensorType, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class BatteryLevel(models.Model):
    voltage = models.FloatField()
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)