from django.contrib import admin
from sensors.models import Sensor, SensorType, BatteryLevel
# Register your models here.
admin.site.register(Sensor)
admin.site.register(SensorType)
admin.site.register(BatteryLevel)