import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: () => import("./views/Dashboard.vue")
    },
    {
      path: "/weather",
      name: "weather",
      component: () => import("./views/Weather.vue")
    },
    {
      path: "/beds",
      name: "beds",
      component: () => import("./views/Beds.vue")
    },
    {
      path: "/vegetables",
      name: "vegetables",
      component: () => import("./views/Vegetables.vue")
    },
    {
      path: "/statistics",
      name: "statistics",
      component: () => import("./views/Statistics.vue")
    },
    {
      path: "/sensors",
      name: "sensors",
      component: () => import("./views/Sensors.vue")
    },
    {
      path: "*",
      redirect: "/404"
    },
    {
      path: "/404",
      name: "page-not-found",
      component: () => import("./views/NotFound.vue")
    }
  ]
});
