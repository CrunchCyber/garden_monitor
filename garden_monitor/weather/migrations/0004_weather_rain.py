# Generated by Django 2.2.6 on 2019-10-14 10:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weather', '0003_auto_20191014_1216'),
    ]

    operations = [
        migrations.AddField(
            model_name='weather',
            name='rain',
            field=models.BooleanField(default=False),
        ),
    ]
