from django.db import models


class Weather(models.Model):
    sensor = models.ForeignKey('sensors.Sensor', on_delete=models.CASCADE)
    temperature = models.FloatField()
    humidity = models.FloatField()
    pressure = models.FloatField()
    brightness = models.FloatField()
    rain = models.BooleanField(default=False)
    rain_amount = models.FloatField(blank=True, default=0)
    wind_speed = models.FloatField()
    wind_direction = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)

