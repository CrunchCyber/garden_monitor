from django.urls import path
from weather.api.views import (WeatherAPIView, WeatherBySensorAPIView, WeatherBySensorLatestAPIView,
                               WeatherBySensorYear, WeatherBySensorYearMonth, WeatherBySensorYearMonthDay)

urlpatterns = [
    path('weather/', WeatherAPIView.as_view(), name='weather-list'),
    path('weather/<int:sensor>/', WeatherBySensorAPIView.as_view(), name='weather-by-sensor'),
    path('weather/<int:sensor>/latest/', WeatherBySensorLatestAPIView.as_view(), name='weather-by-sensor-latest'),
    path('weather/<int:sensor>/<int:year>/', WeatherBySensorYear.as_view(), name='weather-by-sensor-year'),
    path('weather/<int:sensor>/<int:year>/<int:month>/', WeatherBySensorYearMonth.as_view(), name='weather-by-sensor'),
    path('weather/<int:sensor>/<int:year>/<int:month>/<int:day>/', WeatherBySensorYearMonthDay.as_view(), name='weather-by-sensor'),
]