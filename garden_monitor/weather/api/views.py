from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from weather.models import Weather
from weather.api.serializers import WeatherSerializer


class WeatherAPIView(generics.ListCreateAPIView):
    queryset = Weather.objects.all()
    serializer_class = WeatherSerializer


class WeatherBySensorAPIView(APIView):
    serializer_classes = WeatherSerializer

    def get(self, request, sensor):
        reports = Weather.objects.filter(sensor=sensor).order_by('-created_at')
        serializer = WeatherSerializer(reports, many=True)
        return Response(serializer.data)


class WeatherBySensorLatestAPIView(APIView):
    serializer_class = WeatherSerializer

    def get(self, request, sensor):
        try:
            report = Weather.objects.filter(sensor=sensor).order_by('-created_at')[0]
            serializer = WeatherSerializer(report)
            return Response(serializer.data)
        except IndexError:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class WeatherBySensorYear(APIView):
    serializer_class = WeatherSerializer

    def get(self, request, sensor, year):
        reports = Weather.objects.filter(sensor=sensor, created_at__year=year)
        serializer = WeatherSerializer(reports, many=True)
        return Response(serializer.data)


class WeatherBySensorYearMonth(APIView):
    serializer_class = WeatherSerializer

    def get(self, request, sensor, year, month):
        reports = Weather.objects.filter(sensor=sensor, created_at__year=year, created_at__month=month)
        serializer = WeatherSerializer(reports, many=True)
        return Response(serializer.data)


class WeatherBySensorYearMonthDay(APIView):
    serializer_class = WeatherSerializer

    def get(self, request, sensor, year, month, day):
        reports = Weather.objects.filter(sensor=sensor, created_at__year=year, created_at__month=month, created_at__day=day)
        serializer = WeatherSerializer(reports, many=True)
        return Response(serializer.data)
