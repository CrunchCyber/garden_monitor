import requests
import statistics
import datetime
import json
import sys


def post_data(payload):
    request_post_url = "http://192.168.1.32:8000/api/statistics/weather/"
    requests.post(request_post_url, data=payload)


def get_last_month():
    today = datetime.datetime.today()
    first = today.replace(day=1)
    lastMonth = first - datetime.timedelta(days=1)
    today = datetime.datetime(lastMonth.year, lastMonth.month, 1)
    return today


def get_weather_data(get_url, last_month):
    payload = {}
    temperature = []
    rain_amount, rain_days, lightning_strikes = -1,-1,-1
    day_to_check = ''
    found_rain = False

    response = requests.get(get_url)
    data = response.json()
        
    for report in data:
        temperature.append(report['temperature'])
        rain_amount += report['rain_amount']

        # set day to check to current day 
        if day_to_check is not report['created_at']:
            day_to_check = report["created_at"]
            found_rain = False

        # check if it rained that day
        if found_rain is False and report['rain']:
            found_rain = True
            rain_days += 1

    payload['date'] = last_month.isoformat()
    payload['temperature_max'] = max(temperature)
    payload['temperature_min'] = min(temperature)
    payload['temperature_median'] = statistics.median(temperature)
    payload['rain_amount'] = rain_amount
    payload['rain_days'] = rain_days
    payload['lightning_strikes'] = lightning_strikes
    return payload

def send_create_weather_stats():

    # get last month
    last_month = get_last_month()

    request_get_url = f'http://192.168.1.32:8000/api/weather/1/2019/{last_month.month}/'

    payload = get_weather_data(request_get_url, last_month)
    post_data(payload)
    sys.exit(0)
