import sys
from monthlyWeatherStats import monthlyWeatherStats


def main():
    if(sys.argv[1] == 'monthlyWeatherStats'):
       monthlyWeatherStats.send_create_weather_stats()


if __name__ == "__main__":
    main()
